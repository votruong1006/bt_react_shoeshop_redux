import React, { Component } from 'react'
import Cart_Shoe from './Cart_Shoe'
import List_Shoe from './List_Shoe'

export default class Shoe_Shop extends Component {
    render() {
        return (
            <div className='container'>
                <h2>Shoe_Shop</h2>
                <div className="row">
                    <div className="col-6"><List_Shoe /></div>
                    <div className="col-6"><Cart_Shoe /></div>
                </div>

            </div>
        )
    }
}
