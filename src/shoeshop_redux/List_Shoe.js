import React, { Component } from 'react'
import { connect } from 'react-redux'
import Item_Shoe from './Item_Shoe'

class List_Shoe extends Component {
    render() {
        return (
            <div className='row'>{this.props.list.map((item) => {
                return <Item_Shoe shoe={item} />
            })}</div>
        )
    }
}
let mapStateToProps = (state) => {
    return { list: state.shoeReducer.listShoe }
}
export default connect(mapStateToProps)(List_Shoe)