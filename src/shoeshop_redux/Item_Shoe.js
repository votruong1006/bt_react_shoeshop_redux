import React, { Component } from 'react'
import { connect } from 'react-redux';
import { ADD_TO_CART } from './redux/constant/shoeConstant';

class Item_Shoe extends Component {
    render() {
        let { image, name, price } = this.props.shoe;
        return (
            <div className="col-3">
                <div className="card">
                    <img src={image} className="card-img-top" alt="..." />
                    <div className="card-body">
                        <h5 className="card-title">{name}</h5>
                        <p className="card-text">{price}</p>
                        <a
                            onClick={() => {
                                this.props.handleAddToCart(this.props.shoe);
                            }}
                            href="#"
                            className="btn btn-primary"
                        >
                            Add
                        </a>
                    </div>
                </div>
            </div>
        )
    }
}
let mapDispatchToProps = (dispatch) => {
    return {
        handleAddToCart: (shoe) => {
            let action = {
                type: ADD_TO_CART,
                payload: shoe
            }
            dispatch(action)
        }
    }
}
export default connect(null, mapDispatchToProps)(Item_Shoe)