import React, { Component } from 'react'
import { connect } from 'react-redux';
import { CHANGE_QUATITY, DELETE_CART } from './redux/constant/shoeConstant';

class Cart_Shoe extends Component {
    renderTbody = () => {
        return this.props.cart.map((item) => {
            return (
                <tr>
                    <td>{item.id}</td>
                    <td>{item.name}</td>
                    <td>
                        <button onClick={() => { this.props.handleChangeQuatity(item.id, -1) }} className="btn btn-danger">-</button>
                        <strong>{item.soLuong}</strong>
                        <button onClick={() => { this.props.handleChangeQuatity(item.id, 1) }} className="btn btn-danger">+</button>
                    </td>
                    <td>{item.price * item.soLuong}</td>
                    <td>
                        <img style={{ width: 50 }} src={item.image} alt="" />
                    </td>
                    <td>
                        <button
                            onClick={() => {
                                this.props.handleDelete(item.id);
                            }}
                            className="btn btn-danger"
                        >
                            Delete
                        </button>
                    </td>
                </tr>
            );
        });
    };
    render() {
        return (
            <div>
                <table className="table">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Quatity</th>
                            <th>Price</th>
                            <th>Img</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>{this.renderTbody()}</tbody>
                </table>
            </div>
        )
    }
}
let mapStateToProp = (state) => {
    return {
        cart: state.shoeReducer.cart
    }
}
let mapDispatchToProps = (dispatch) => {
    return {
        handleDelete: (idShoe) => {
            let action = {
                type: DELETE_CART,
                payload: idShoe
            }
            dispatch(action)
        },
        handleChangeQuatity: (idShoe, luaChon) => {
            let action = {
                type: CHANGE_QUATITY,
                payload: { idShoe, luaChon }
            }
            dispatch(action)
        }
    }
}
export default connect(mapStateToProp, mapDispatchToProps)(Cart_Shoe)